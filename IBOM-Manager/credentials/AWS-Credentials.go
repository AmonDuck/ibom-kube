package credentials

type AWSCredentials struct {
	Name            string `binding:"required"`
	Description     string
	AccessKey       string `binding:"required"`
	SecretAccessKey string `binding:"required"`
	Region          string `binding:"required"`
	SessionToken    string
}

func (credentials *AWSCredentials) GetCloudType() string {
	return "AWS"
}

func (credentials *AWSCredentials) GetName() string {
	return credentials.Name
}

func (credentials *AWSCredentials) GetDescription() string {
	return credentials.Description
}

func (credentials *AWSCredentials) GetAccessKey() string {
	return credentials.AccessKey
}

func (credentials *AWSCredentials) GetSecretAccessKey() string {
	return credentials.SecretAccessKey
}

func (credentials *AWSCredentials) GetRegion() string {
	return credentials.Region
}

func (credentials *AWSCredentials) GetSessionToken() string {
	return credentials.SessionToken
}
