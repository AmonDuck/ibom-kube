package credentials

import (
	"IBOM-Manager/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"os"
)

func GetAllCredentials() []Credential {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
	})
	if err != nil {
		utils.Logger.Error.Println("Failed to create session")
	}

	svc := dynamodb.New(sess)

	tableName := "RegisterCloudsDB"
	params := &dynamodb.ScanInput{
		TableName: aws.String(tableName),
	}

	type FoundCloudType struct {
		CloudType string
	}

	result, err := svc.Scan(params)
	if err != nil {
		utils.Logger.Error.Println("Failed to scan database")
	}

	foundCredentials := []Credential{}

	for _, item := range result.Items {
		foundCloudType := FoundCloudType{}

		err = dynamodbattribute.UnmarshalMap(item, &foundCloudType)
		if err != nil {
			utils.Logger.Error.Println("Unmarshalling failed in GetAllCredentials")
		}

		switch foundCloudType.CloudType {
		case "AmazonCloud":
			awsCredentials := AWSCredentials{}
			err = dynamodbattribute.UnmarshalMap(item, &awsCredentials)
			if err != nil {
				utils.Logger.Error.Println("Unmarshalling failed in AWSCredentials")
			}

			foundCredentials = append(
				foundCredentials,
				&awsCredentials,
			)
		case "GoogleCloud":
			//TODO: implement GoogleCloud, not required for our PoC
			//append(foundCredentials, )
		case "AzureCloud":
			//TODO: implement AzureCloud, not required for our PoC
			//append(foundCredentials, )
		default:
			utils.Logger.Error.Println("Unsupported cloud type: " + foundCloudType.CloudType)
		}
	}

	return foundCredentials
}
