package credentials

type Credential interface {
	GetCloudType() string
	GetName() string
	GetDescription() string
}
