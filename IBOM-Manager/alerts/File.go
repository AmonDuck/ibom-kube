package alerts

type FileInfos struct {
	File_Infos []File `json:"file_infos"`
}

type File struct {
	ID string `json:"id"`
}
