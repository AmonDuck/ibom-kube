package alerts

import (
	"IBOM-Manager/utils"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
)

func AlertUsersOfVulnerability(vulnerabilityPdfData []byte) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
	})
	if err != nil {
		utils.Logger.Error.Println("Failed to create session")
	}

	svc := dynamodb.New(sess)

	tableName := "AlertDB"
	//TODO: To optimize later with Global Secondary Index to find all users with a certain severity subscription
	params := &dynamodb.ScanInput{
		TableName: aws.String(tableName),
	}

	result, err := svc.Scan(params)
	if err != nil {
		utils.Logger.Error.Println("Failed to scan database")
	}

	for _, item := range result.Items {

		toInformUser := ToInformUser{}

		err = dynamodbattribute.UnmarshalMap(item, &toInformUser)
		if err != nil {
			utils.Logger.Error.Println("Unmarshalling failed:", err)
		}

		sendMatterMostReport(toInformUser, &vulnerabilityPdfData)

	}
}

func sendMatterMostReport(toInformUser ToInformUser, toSendPDF *[]byte) {
	token, userId, err := getMatterMostToken(toInformUser)
	if err != nil {
		utils.Logger.Error.Println("Failed to get matter most token:", err)
		return
	}

	fileIdsList, err := uploadFileToMatterMost(toInformUser, token, toSendPDF)
	if err != nil {
		utils.Logger.Error.Println("There was an error uploading the file(s):\n", err)
		return
	}

	err = sendMatterMostMessage(toInformUser, fileIdsList, userId, token)
	if err != nil {
		utils.Logger.Error.Println("Failed to send message: \n", err)
	}
}

func getMatterMostToken(toInformUser ToInformUser) (string, UserId, error) {
	url := "https://" + toInformUser.Domain + "/api/v4/users/login"
	body := map[string]string{
		"login_id": toInformUser.Login_ID,
		"password": toInformUser.Password,
		"token":    "",
		"deviceId": "",
	}

	jsonData, err := json.Marshal(body)
	if err != nil {
		utils.Logger.Error.Println("Error marshalling JSON:", err)
		return "", UserId{}, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		utils.Logger.Error.Println("Error creating login request:", err)
		return "", UserId{}, err
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		utils.Logger.Error.Println("Error executing login request:", err)
		return "", UserId{}, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Logger.Error.Println("Error reading response body:", err)
		return "", UserId{}, err
	}

	token := resp.Header.Get("token")
	var userId UserId
	err = json.Unmarshal(bodyBytes, &userId)
	if err != nil {
		utils.Logger.Error.Println("Error decoding JSON:", err)
		return "", UserId{}, err
	}

	return token, userId, nil
}

func uploadFileToMatterMost(toInformUser ToInformUser, token string, toSendPDF *[]byte) ([]string, error) {
	url := "https://" + toInformUser.Domain + "/api/v4/files"

	bbuff := &bytes.Buffer{}
	multiPartWriter := multipart.NewWriter(bbuff)

	fieldWriter, err := multiPartWriter.CreateFormField("channel_id")
	if err != nil {
		fmt.Println("Error creating form field:", err)
		return nil, err
	}
	_, err = io.Copy(fieldWriter, strings.NewReader(toInformUser.Channel_ID))
	if err != nil {
		fmt.Println("Error copying channel ID:", err)
		return nil, err
	}

	fileWriter, err := multiPartWriter.CreateFormFile("files", "rapport.pdf")
	if err != nil {
		fmt.Println("Error creating form file:", err)
		return nil, err
	}
	_, err = io.Copy(fileWriter, bytes.NewReader(*toSendPDF))
	if err != nil {
		fmt.Println("Error copying file:", err)
		return nil, err
	}

	err = multiPartWriter.Close()
	if err != nil {
		fmt.Println("Error closing multipart writer:", err)
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bbuff)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return nil, err
	}

	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, //TODO: DO NOT USE THIS IN PRODUCTION!!!
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		utils.Logger.Error.Println("Error sending request:", err)
		return nil, err
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		utils.Logger.Error.Println("Error reading response body:", err)
		return nil, err
	}

	var fileInfos FileInfos
	err = json.Unmarshal(bodyBytes, &fileInfos)
	if err != nil {
		utils.Logger.Error.Println("Error decoding JSON:", err)
		return nil, err
	}

	fileIds := make([]string, 0)
	for _, file := range fileInfos.File_Infos {
		fileIds = append(fileIds, file.ID)
	}

	return fileIds, nil
}

func sendMatterMostMessage(toInformUser ToInformUser, fileIds []string, userId UserId, token string) error {
	url := "https://" + toInformUser.Domain + "/api/v4/posts"
	body := map[string]any{
		"message":    "EEN VULNERABILITY IS GEVONDEN!",
		"channel_id": toInformUser.Channel_ID,
		"user_id":    userId.User_ID,
		"file_ids":   fileIds,
	}

	jsonData, err := json.Marshal(body)
	if err != nil {
		utils.Logger.Error.Println("Error marshalling JSON:", err)
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		utils.Logger.Error.Println("Error creating post message request:", err)
		return err
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		utils.Logger.Error.Println("Error executing post message request:", err)
		return err
	}

	if resp.StatusCode != 200 && resp.StatusCode != 201 {
		utils.Logger.Error.Println("Received StatusCode: ", resp.StatusCode)
	}

	return nil
}
