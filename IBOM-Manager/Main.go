package main

import (
	"IBOM-Manager/credentials"
	"IBOM-Manager/iacGenerators"
	"IBOM-Manager/iacScanning"
	"IBOM-Manager/utils"
	"IBOM-Manager/worker"
	"errors"
	"log"
	"os"
)

func main() {
	Init()
	var credentialList []credentials.Credential = credentials.GetAllCredentials()
	scanners := []iacScanning.IACScanner{iacScanning.LocalKicsScan}
	worker.Worker(credentialList, iacGenerators.GenerateIaC, scanners)
}

func Init() {
	file, err := os.OpenFile("log.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	utils.Logger.Error = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	utils.Logger.Warning = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	utils.Logger.Info = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)

	//Uncommenting these lines will switch logging output to console:
	utils.Logger.Error.SetOutput(os.Stderr)
	utils.Logger.Warning.SetOutput(os.Stderr)
	utils.Logger.Info.SetOutput(os.Stdout)

	worker.Tempdir, err = os.MkdirTemp("", "IBOM-manager-*")
	if err != nil {
		utils.Logger.Error.Fatal(err)
	}
}

func checkFileExists(filePath string) bool {
	_, error := os.Stat(filePath)
	//return !os.IsNotExist(err)
	return !errors.Is(error, os.ErrNotExist)
}
