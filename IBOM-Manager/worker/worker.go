package worker

import (
	"IBOM-Manager/alerts"
	"IBOM-Manager/credentials"
	"IBOM-Manager/domain"
	"IBOM-Manager/iacGenerators"
	"IBOM-Manager/iacScanning"
	"IBOM-Manager/utils"
	"log"
	"os"
	"path/filepath"
	"time"
)

var Tempdir string

func Worker(credentialList []credentials.Credential, iacgenerator iacGenerators.IaCgenerator, scanners []iacScanning.IACScanner) {
	utils.Logger.Info.Println("Starting worker with list size: ", len(credentialList))
	//TODO: tune the buffer size
	toBeScannedStacksChannel := make(chan *domain.Stack, 20)

	go generateIaC(credentialList, toBeScannedStacksChannel, iacgenerator)

	reportsChannel := make(chan *[]byte)

	go scanIaC(toBeScannedStacksChannel, scanners, Tempdir, reportsChannel)

	for report := range reportsChannel {
		alerts.AlertUsersOfVulnerability(*report)
	}

}

func generateIaC(credentialList []credentials.Credential, channel chan *domain.Stack, iacgenerator iacGenerators.IaCgenerator) {
	utils.Logger.Info.Println("Generating IaC...")

	//We initialize a logger so we can atomically write credentials to the credential file
	home, err := os.UserHomeDir()
	if err != nil {
		utils.Logger.Error.Println("Failed to retrieve home directory")
		return
	}

	filePath := filepath.Join(home, ".aws", "credentials")

	f, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	credentialFile := log.New(f, "", 0)

	counter := utils.NewThreadSafeCounter(len(credentialList))
	for _, credential := range credentialList {
		go iacgenerator(credential, credentialFile, Tempdir, channel, counter)
	}
	for counter.Value() > 0 {
		time.Sleep(1 * time.Second)
	}

	//os.Remove("~/.aws/credentials")
	close(channel)
}

func scanIaC(toBeScannedChannel chan *domain.Stack, scanners []iacScanning.IACScanner, tempdir string, reportsChannel chan *[]byte) {
	//	TODO: add parallelization when horizontal scaling for scanning becomes possible
	for stack := range toBeScannedChannel {
		for _, scanner := range scanners {
			_, report, err := scanner(stack, tempdir)
			if err != nil {
				utils.Logger.Error.Println(err)
			}
			if report != nil {
				reportsChannel <- &report
			}
		}
	}
	close(reportsChannel)
}
