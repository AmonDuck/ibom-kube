package domain

type Stack struct {
	name            string
	iac             infrastructureAsCode
	vulnerabilities []Vulnerability
}

func NewStack(name string, templateAsString string) *Stack {
	return &Stack{
		name: name,
		iac:  infrastructureAsCode{templateAsString},
	}
}

func (stack *Stack) UpdateInfrastructureAsCode(newIaC string) {
	stack.iac = infrastructureAsCode{newIaC}
}

func (stack *Stack) GetInfrastructureAsCode() string {
	return stack.iac.content
}

func (stack *Stack) SetName(name string) {
	stack.name = name
}

func (stack *Stack) ReplaceVulnerabilities(vulnerabilities []Vulnerability) {
	stack.vulnerabilities = vulnerabilities
}

func (stack *Stack) GetName() string {
	return stack.name
}
