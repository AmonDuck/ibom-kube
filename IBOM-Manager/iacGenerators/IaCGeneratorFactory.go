package iacGenerators

import (
	"IBOM-Manager/credentials"
	"IBOM-Manager/domain"
	"IBOM-Manager/utils"
	"fmt"
	"github.com/google/uuid"
	"log"
)

type IaCgenerator func(credential credentials.Credential, credentialFile *log.Logger, tempdir string, outputChannel chan *domain.Stack, counter *utils.ThreadSafeCounter)

// Returns file location for IaC file and an ok status
func GenerateIaC(credential credentials.Credential, credentialFile *log.Logger, tempdir string, outputChannel chan *domain.Stack, counter *utils.ThreadSafeCounter) {
	defer counter.Decrement()

	id := uuid.New()

	switch credential.(type) {

	case *credentials.AWSCredentials:
		awscredential := *credential.(*credentials.AWSCredentials)
		credentialString := fmt.Sprintf(
			"\n[%s]\n"+
				"aws_access_key_id=%s\n"+
				"aws_secret_access_key=%s\n",
			id.String(), awscredential.GetAccessKey(), awscredential.GetSecretAccessKey())
		if awscredential.SessionToken != "" {
			credentialString = fmt.Sprintf(
				credentialString,
				"aws_session_token=%s\n",
				awscredential.GetSessionToken())
		}
		credentialFile.Println(credentialString)

		outputChannel <- domain.NewStack(awscredential.GetName(), GetAWSIaC(awscredential, id, tempdir))

	default:
		utils.Logger.Error.Println("Unknown cloud: ", credential.GetCloudType(), ", of type: ", credential.GetCloudType())
		return
	}

}
