package iacGenerators

import (
	credentials "IBOM-Manager/credentials"
	"IBOM-Manager/utils"
	"github.com/google/uuid"
	"os"
	"os/exec"
)

// returns a filelocation to a yml file
func GetAWSIaC(awsCredentials credentials.AWSCredentials, id uuid.UUID, tempdir string) string {
	fileName := tempdir + "/iac_" + id.String() + ".yml"

	command := exec.Command("former2", "generate", "--output-cloudformation", fileName, "--profile", id.String(), "--region", awsCredentials.Region)

	err := command.Start()
	if err != nil {
		utils.Logger.Error.Println(err)
	}

	err = command.Wait()
	if err != nil {
		utils.Logger.Error.Println(err)
	}
	utils.Logger.Info.Println("Finished generating IaC!")
	content, err := os.ReadFile(fileName)
	if err != nil {
		utils.Logger.Error.Println(err)
	}
	os.Remove(fileName)
	return string(content)
}
