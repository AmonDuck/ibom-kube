package utils

import "sync"

type ThreadSafeCounter struct {
	mutex   sync.Mutex
	counter int
}

func NewThreadSafeCounter(initValue int) *ThreadSafeCounter {
	return &ThreadSafeCounter{counter: initValue}
}

func (threadSafeCounter *ThreadSafeCounter) Decrement() {
	threadSafeCounter.mutex.Lock()
	defer threadSafeCounter.mutex.Unlock()
	threadSafeCounter.counter--
}

func (threadSafeCounter *ThreadSafeCounter) Value() int {
	threadSafeCounter.mutex.Lock()
	defer threadSafeCounter.mutex.Unlock()
	return threadSafeCounter.counter
}
