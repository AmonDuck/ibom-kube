package utils

import (
	"log"
	"os"
)

var Logger loggers = loggers{
	Error:   log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile),
	Info:    log.New(os.Stderr, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile),
	Warning: log.New(os.Stdout, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile),
}

type loggers struct {
	Error   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
}
