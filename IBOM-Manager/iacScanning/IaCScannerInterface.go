package iacScanning

import "IBOM-Manager/domain"

// returns a slice of vulnerabilities, a potential report and a potential error
type IACScanner func(stack *domain.Stack, tempDir string) ([]domain.Vulnerability, []byte, error)
