package iacScanning

import (
	"IBOM-Manager/domain"
	"IBOM-Manager/utils"
	"fmt"
	"github.com/google/uuid"
	"os"
	"os/exec"
)

// This is a function for scanning with the local kics install inside the same docker container,
// in the future there should be aimed to provide a seperate kics installation that can be used to scan for vulnerabilities
// Thi is especially important if one ever wishes to implement horizontal scaling.
func LocalKicsScan(stack *domain.Stack, tempDir string) ([]domain.Vulnerability, []byte, error) {
	fmt.Println("starting kics scan")
	outputPath := tempDir + "/" + uuid.New().String()
	inputFile := tempDir + "/" + uuid.New().String() + ".yml"
	err := os.WriteFile(inputFile, []byte(stack.GetInfrastructureAsCode()), 777)
	if err != nil {
		return nil, nil, err
	}
	command := exec.Command("/kics/bin/kics", "scan", "-p", inputFile, "--report-formats", "json,pdf", "--ignore-on-exit", "results", "--queries-path", "/kics/assets/queries", "-o", outputPath)

	//used for reading console output
	//output, _ := command.StdoutPipe()
	//erroroutput, _ := command.StderrPipe()

	err = command.Start()

	//used for reading console output
	//go readOutput(output)
	//go readOutput(erroroutput)

	if err != nil {
		return nil, nil, err
	}
	err = command.Wait()

	if err != nil {
		return nil, nil, err
	}

	report, err := os.ReadFile(outputPath + "/results.pdf")
	if err != nil {
		return nil, nil, err
	}
	output, _ := os.ReadFile(outputPath + "/results.json")
	vulnerabilities := processJson(string(output))

	return *vulnerabilities, report, nil
}

// not done
func processJson(output string) *[]domain.Vulnerability {
	utils.Logger.Info.Println(output)
	emptylist := make([]domain.Vulnerability, 0)
	return &emptylist
}

//used for reading console output
/*func readOutput(output io.ReadCloser) {
	scanner := bufio.NewScanner(output)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		m := scanner.Text()
		fmt.Println(m)
	}
}*/
