module IBOM-Manager

go 1.22

require github.com/aws/aws-sdk-go v1.53.10

require github.com/jmespath/go-jmespath v0.4.0 // indirect

require github.com/google/uuid v1.6.0
