package main

import (
	"IBOMKube/server"
)

func main() {
	server := server.NewServer()

	server.Run()
}
