package routes

import (
	"IBOMKube/presentation"
	"github.com/gin-gonic/gin"
)

func ConfigRoutes(router *gin.Engine) *gin.Engine {
	main := router.Group("api/v1")
	{
		register := main.Group("register")
		{
			register.POST("/", presentation.RegisterCloud)
			//books.GET("/", controllers.GetAllBooks)
			//books.POST("/", controllers.CreateBook)
			//books.DELETE("/:id", controllers.DeleteBook)
			//books.PUT("/", controllers.UpdateBook)
		}
	}

	return router
}
