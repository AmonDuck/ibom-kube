package application

import (
	"IBOMKube/domain"
	"bytes"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"io/ioutil"
)

func RegisterCloud(c *gin.Context) {
	bodyAsByteArray, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(400, gin.H{"message": "Failed to read request body"})
		return
	}

	bodyCopy := ioutil.NopCloser(bytes.NewBuffer(bodyAsByteArray))

	var jsonMap map[string]interface{}
	if err := json.Unmarshal(bodyAsByteArray, &jsonMap); err != nil {
		c.JSON(400, gin.H{"message": "Failed to parse JSON"})
		return
	}

	cloudType, ok := jsonMap["cloudtype"].(string)
	if !ok {
		c.JSON(400, gin.H{"message": "cloudtype is required"})
		return
	}

	uuid := uuid.New()
	var newCloud interface{}

	switch cloudType {
	case "AmazonCloud":
		var awsCloud domain.AWSCloud
		c.Request.Body = bodyCopy
		if err := c.ShouldBindJSON(&awsCloud); err != nil {
			c.JSON(400, gin.H{"message": "Failed to bind JSON for AmazonCloud"})
			return
		}
		awsCloud.PK = "CLOUD#" + uuid.String()
		awsCloud.SK = "CLOUDTYPE#" + cloudType
		awsCloud.ID = uuid.String()
		newCloud = awsCloud
	case "GoogleCloud":
		var googleCloud domain.GoogleCloud
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyAsByteArray))
		if err := c.ShouldBindJSON(&googleCloud); err != nil {
			c.JSON(400, gin.H{"message": "Failed to bind JSON for GoogleCloud"})
			return
		}
		googleCloud.PK = "CLOUD#" + uuid.String()
		googleCloud.SK = "CLOUDTYPE#" + cloudType
		googleCloud.ID = uuid.String()
		newCloud = googleCloud
	case "AzureCloud":
		var azureCloud domain.AzureCloud
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyAsByteArray))
		if err := c.ShouldBindJSON(&azureCloud); err != nil {
			c.JSON(400, gin.H{"message": "Failed to bind JSON for AzureCloud"})
			return
		}
		azureCloud.PK = "CLOUD#" + uuid.String()
		azureCloud.SK = "CLOUDTYPE#" + cloudType
		azureCloud.ID = uuid.String()
		newCloud = azureCloud
	default:
		c.JSON(400, gin.H{"message": "Invalid cloudtype"})
		return
	}

	validateStruct(c, newCloud)

	saveToDynamoDB(c, newCloud)
	c.JSON(200, gin.H{"message": "Cloud registered successfully"})
}

func saveToDynamoDB(c *gin.Context, cloud interface{}) {
	sess, err := session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	})
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Failed to create a session with DynamoDB",
		})
		return
	}

	svc := dynamodb.New(sess)

	attributeValue, err := dynamodbattribute.MarshalMap(cloud)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Got error marshalling new cloud item",
		})
		return
	}

	tableName := "RegisterCloudsDB"

	_, err = svc.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String(tableName),
		Item:      attributeValue,
	})

	if err != nil {
		c.JSON(500, gin.H{
			"message": "Failed to put item into DynamoDB" + err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"message": "Successfully saved to DynamoDB",
	})
}

func validateStruct(c *gin.Context, toValidateStruct interface{}) {
	validate := validator.New()

	err := validate.Struct(toValidateStruct)
	if err != nil {
		c.JSON(400, gin.H{
			"message": "Validation failed on Struct",
		})
		return
	}
}
