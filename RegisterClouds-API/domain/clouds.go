package domain

type AWSCloud struct {
	PK              string
	SK              string
	ID              string `json:"id"`
	Name            string `json:"name" binding:"required"`
	Description     string `json:"description"`
	CloudType       string `json:"cloudtype" binding:"required"`
	AccessKey       string `json:"accesskey" binding:"required"`
	SecretAccessKey string `json:"secretaccesskey" binding:"required"`
	Region          string `json:"region" binding:"required"`
	SessionToken    string `json:"sessiontoken"`
}

// TODO: Voor een later team om uit te breiden voor Azure
type AzureCloud struct {
	PK          string
	SK          string
	ID          string
	Name        string `json:"name" binding:"required"`
	Description string `json:"description"`
	Credential  string `json:"credential" binding:"required"`
	CloudType   string `json:"cloudtype" binding:"required" validate:"oneof= AmazonCloud GoogleCloud AzureCloud"`
}

// TODO: Voor een later team om uit te breiden voor Google
type GoogleCloud struct {
	PK          string
	SK          string
	ID          string
	Name        string `json:"name" binding:"required"`
	Description string `json:"description"`
	Credential  string `json:"credential" binding:"required"`
	CloudType   string `json:"cloudtype" binding:"required" validate:"oneof= AmazonCloud GoogleCloud AzureCloud"`
}
