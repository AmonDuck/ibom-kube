package presentation

import (
	"IBOMKube/application"
	"github.com/gin-gonic/gin"
)

func RegisterCloud(c *gin.Context) {
	application.RegisterCloud(c)
}
