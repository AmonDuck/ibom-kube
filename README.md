# IBOM-Kube

## C1 (System Context Diagram)

![alt text](<docs/c4-model/IBOM - System Context diagram.png>)

## C2 (Container Diagram)

![alt text](<docs/c4-model/IBOM - Containers C2.png>)

# IBOM-Manager

I recommend installing the plantUML intellij extention if you're working on this.

```plantuml
actor "Cron-job" as actor
participant main as main
participant credenital_retriever as credenital_retriever
participant worker as worker
participant alerts as alerts
participant MatterMost as MatterMost
participant scanIaC as scanIaC
participant generateIaC as generateIaC
participant IaCGenerator as iacgenerator

actor -> main : Start IBOM-Manager
activate main
main -> credenital_retriever : getAllCredentials()
activate credenital_retriever
deactivate credenital_retriever
main -> worker : worker(Credentials[])
activate worker

worker -> generateIaC : go generateIaC(Credentials[], iacGenerators.GenerateIaC, scanners []IACScanner)
activate generateIaC
loop range Credenitial
    generateIaC -> iacgenerator : go generateIaC()
    activate iacgenerator
end
deactivate generateIaC

worker -> scanIaC : go scanIaC()
activate scanIaC
iacgenerator -> scanIaC : channel iac
deactivate iacgenerator
loop for range iac from channel
    scanIaC -> iacScanner : scanIaC()
    activate iacScanner
    deactivate iacScanner
end
deactivate iacgenerator

    scanIaC -> worker : channel report
    deactivate scanIaC
loop for range report from channel
    deactivate generateIaC
    worker -> alerts : AlertUserOfVulnerability(*report)
    activate alerts
    alerts -> MatterMost : getToken 
    alerts -> MatterMost : uploadFile
    alerts -> MatterMost : sendMessage
    deactivate alerts
end
 deactivate worker
```

## Pipelines

We have chosen to use the ['Parent-Child Pipeline'](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#parent-child-pipelines 'What is a mono-repository') architecture because our repository is a [mono-repository](https://semaphoreci.com/blog/what-is-monorepo).

The parent-pipeline is located at [.gitlab-ci.yml](./.gitlab-ci.yml). \
The child-pipelines are located each at its own folder. For example the child-pipeline for IBOM-Manager is located at [./IBOM-Manager/.IBOM-Manager.gitlab-ci.yml](./IBOM-Manager/.IBOM-Manager.gitlab-ci.yml)

### Parent Pipeline

The Parent-Pipeline is responsible for triggering the Child-Pipelines, testing and the deployment (e.g. to Kubernetes). The test and deployment stages of the parent-pipeline is dependent on the child-pipelines. The test and deployment stages won't run before the trigger stage which is responsible for triggering the child-pipelines.

![Parent-Pipeline](docs/pipelines/ParentPipeline-FlowChart.png)

### Child Pipelines

Each child-pipeline could have its own use-case. In our use-case the steps for all the containers are the same which explains why all the child-pipelines look the same.

The child-pipelines have the test and build stage.\
At the test stage the GO code is tested on different GO specific tests and checks for formats. If the test stage fails the Docker image will not be build.

If all the tests are complete and correct the next stage will be run, which is the building stage. In this stage a Docker image is build based on the 'Dockerfile'. After the image is build it will be published in the Container Registry of GitLab.

![Child-Pipeline](docs/pipelines/ChildPipeline-FlowChart.png)
