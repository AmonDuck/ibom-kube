package application

import (
	"github.com/gin-gonic/gin"
)

func SubscribeForAlerts(c *gin.Context) {

	c.JSON(200, gin.H{"message": "Subscribed for alerts successfully"})
}
