package routes

import (
	"Alert-API/presentation"
	"github.com/gin-gonic/gin"
)

func ConfigRoutes(router *gin.Engine) *gin.Engine {
	main := router.Group("api/v1")
	{
		register := main.Group("alert")
		{
			register.POST("/", presentation.AlertUsers)
		}
	}

	return router
}
