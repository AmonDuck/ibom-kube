package presentation

import (
	"Alert-API/application"
	"github.com/gin-gonic/gin"
)

func SubsribeForAlerts(c *gin.Context) {
	application.SubscribeForAlerts(c)
}

func AlertUsers(c *gin.Context) {
	application.AlertUsers(c)
}
