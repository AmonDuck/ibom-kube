package domain

type User struct {
	PK              string
	SK              string
	ID              string
	Name            string
	Email           string
	PhoneNumber     string `json:"phonenumber"`
	MinimumSeverity string `json:"minimumseverity" binding:"required" validate:"oneof= LOW MEDIUM HIGH CRITICAL"`
}
